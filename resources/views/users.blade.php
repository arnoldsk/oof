@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @foreach($users as $user)
        <div class="col-md-3 mb-4">
            <div class="card">
                <div class="card-body text-center">
                    <div class="avatar pe-n mb-2">
                        <img src="{{ $user->avatar }}" class="img-circle" alt>
                    </div>
                    <a href="{{ route('profile', ['userId' => $user->id]) }}">
                        @if($user->blocked)
                        <s class="text-dark">{{ $user->username }}</s>
                        @else
                        <span>{{ $user->username }}</span>
                        @endif
                    </a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection
