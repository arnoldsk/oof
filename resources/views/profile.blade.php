@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <h3 class="card-header text-center">
                    <div class="avatar pe-n mb-2">
                        <img src="{{ $user->avatar }}" class="img-circle" alt>
                    </div>
                    @if($user->blocked)
                    <s class="text-dark">{{ $user->username }}</s>
                    @else
                    <span>{{ $user->username }}</span>
                    @endif
                </h3>
                <ul class="list-group list-group-flush">
                    @if($user->payments->count())
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-md-3 text-center">
                                {{ __('Supporter badges') }}
                            </div>
                            <div class="col-md-7 d-flex justify-content-center align-items-center flex-wrap">
                                @foreach($user->payments->sortByDesc('created_at') as $payment)
                                <div class="badge badge-pill badge-success m-1">{{ $payment->product->getNameWithPrefix($payment) }}</div>
                                @endforeach
                            </div>
                        </div>
                    </li>
                    @endif
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-md-3 text-center">
                                {{ __('Discord') }}
                            </div>
                            <div class="col-md-7 d-flex justify-content-center align-items-center flex-wrap">
                                @if($discordUser = $user->discord)
                                <div class="d-flex align-items-center">
                                    <img src="{{ $discordUser->avatar }}?size=32" class="icon icon-circle pe-n mr-2" alt>
                                    <span>{{ $discordUser->username }}#{{ $discordUser->discriminator }}</span>
                                </div>
                                @else
                                <span>-</span>
                                @endif

                                @if($user->id === \Auth::id())
                                <a
                                    class="btn btn-outline-info btn-sm ml-2"
                                    href="{{ route('profile.discord.update') }}"
                                    onclick="event.preventDefault(); document.getElementById('discord-update-form').submit();"
                                >
                                    {{  __('Update') }}
                                </a>

                                <form id="discord-update-form" action="{{ route('profile.discord.update') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                                @endif
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection
