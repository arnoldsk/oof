@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-sm table-striped">
                        <thead>
                            <tr>
                                <th>{{ __('Demo name') }}</th>
                                <th>{{ __('Download') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($filenames as $filename)
                            <tr>
                                <td>
                                    {{ $filename }}
                                </td>
                                <td>
                                    <a href="{{ route('home.demos', ['filename' => $filename]) }}">
                                        {{ __('Download') }}
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
