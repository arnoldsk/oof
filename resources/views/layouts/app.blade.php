<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ isset($title) && !empty($title) ? $title . ' - ' : '' }}{{ config('app.name', 'OOF') }}</title>

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}">

    <!-- Scripts -->
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey={{ app()->config->get('app.tinymce_api_key') }}" defer></script>
    <script src="{{ asset('js/libs.js') }}" defer></script>
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/libs.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/themes/solar.min.css') }}" rel="stylesheet">

    <script>
        const APP_URL = '{{ url('/') }}';
    </script>
</head>
<body>
    <div id="app" v-if="{{ ($user = \Auth::user()) && $user->blocked ? 'false' : 'true' }}">
        <nav class="navbar navbar-expand-md navbar-dark bg-dark">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'OOF') }}
                </a>
                <button
                    class="navbar-toggler"
                    type="button"
                    data-toggle="collapse"
                    data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent"
                    aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}"
                >
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    @include('layouts.navigation-links')
                </div>
            </div>
        </nav>

        <main class="py-4">
            @include('layouts.alerts')

            @yield('content')

            <div class="container">
                <div class="row justify-content-center mt-3">
                    <ul class="nav justify-content-center">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('discord') }}" target="_blank">
                                <small>{{ __('Discord') }}</small>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('steamgroup') }}" target="_blank">
                                <small>{{ __('Steam Group') }}</small>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </main>
    </div>
</body>
</html>
