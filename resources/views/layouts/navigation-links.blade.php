<!-- Left Side Of Navbar -->
<ul class="navbar-nav mr-auto">
    <li class="nav-item">
        <a class="nav-link" href="{{ route('shop') }}">{{ __('Shop') }}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('donate') }}">{{ __('Donate') }}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('home.demos') }}">{{ __('Demos') }}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="https://sb.oof.lv" target="_blank">{{ __('Bans') }}</a>
    </li>
</ul>

<!-- Right Side Of Navbar -->
<ul class="navbar-nav ml-auto">
    <!-- Authentication Links -->
    @guest
        <li class="nav-item">
            <a class="nav-link" href="{{ route('auth') }}">{{ __('Login with Steam') }}</a>
        </li>
    @else
        @if(\Auth::user()->admin)
        <li class="nav-item">
            <a class="nav-link text-danger" href="{{ route('admin.news') }}">{{ __('News') }}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-danger" href="{{ route('admin.users') }}">{{ __('Users') }}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-danger" href="{{ route('admin.payments') }}">{{ __('Payments') }}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-danger" href="{{ route('admin.donations') }}">{{ __('Donations') }}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-danger" href="{{ route('admin.settings') }}">{{ __('Settings') }}</a>
        </li>
        @endif
        <li class="nav-item">
            <a class="nav-link" href="{{ route('users') }}">{{ __('Users') }}</a>
        </li>
        <li class="nav-item dropdown">
            <a
                id="profile-dropdown"
                class="nav-link dropdown-toggle d-flex align-items-center"
                href="#"
                role="button"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
            >
                <img src="{{ Auth::user()->avatar }}" class="icon icon-circle mr-1 pe-n" alt>
                {{ Auth::user()->username }} <span class="caret"></span>
            </a>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="profile-dropdown">
                <a class="dropdown-item" href="{{ route('profile') }}">
                    {{ __('Profile') }}
                </a>
                <a
                    class="dropdown-item"
                    href="{{ route('auth.logout') }}"
                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                >
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('auth.logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
    @endguest
</ul>
