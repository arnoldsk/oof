@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body text-center">
                    <p>{{ __('Consider donating to support the project.') }}</p>

                    <form
                        action="https://www.paypal.com/cgi-bin/webscr"
                        method="post"
                        target="_top"
                        class="mb-3"
                        @submit="onPaypalDonateStart"
                    >
                        <input type="hidden" name="cmd" value="_s-xclick">
                        <input type="hidden" name="hosted_button_id" value="{{ $paypalButtonId }}" v-pre>
                        <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button">
                        <img alt="" border="0" src="https://www.paypal.com/en_LV/i/scr/pixel.gif" width="1" height="1">
                    </form>

                    <p class="text-info">{{ __('Please keep in mind that there are no donation perks.') }}</p>
                    <p>{!! __('Visit the <a href=":url">shop</a> to see perks available for purchase.', ['url' => route('shop')]) !!}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
