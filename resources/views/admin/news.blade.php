@extends('layouts.app')

@section('content')
<div class="container">
    <div class="accordion mb-4" id="news-form-accordion">
        <div class="card">
            <div class="card-header" id="headingOne">
                <h5 class="mb-0">
                    <button
                        class="btn btn-link"
                        type="button"
                        data-toggle="collapse"
                        data-target="#news-form"
                        aria-expanded="true"
                        aria-controls="news-form"
                    >
                        @if($editNewsItem)
                            {{ __('Edit an existing post') }}
                        @else
                            {{ __('Write a new post') }}
                        @endif
                    </button>
                </h5>
            </div>

            <div
                id="news-form"
                class="collapse"
                :class="{ 'show': {{ $editNewsItem ? 'true' : 'false' }} }"
                aria-labelledby="headingOne"
                data-parent="#news-form-accordion"
            >
                <div class="row">
                    <div class="col-md-6">
                        <div class="card-body">
                            <form action="{{ route('admin.news.save') }}" method="post" autocomplete="off">
                                @csrf

                                @if($editNewsItem)
                                    <input type="hidden" name="id" value="{{ $editNewsItem->id }}">
                                @endif

                                <div class="form-group is-invalid">
                                    <label for="news-title">{{ __('Title') }}</label>
                                    <input
                                        type="text"
                                        name="title"
                                        id="news-title"
                                        class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}"
                                        value="{{ $editNewsItem->title ?? old('title') }}"
                                    >
                                    <div class="invalid-feedback">{{ $errors->first('title') }}</div>
                                </div>

                                <div class="form-group">
                                    <label for="news-content">{{ __('Content') }}</label>
                                    <textarea
                                        name="content"
                                        id="news-content"
                                        class="wysiwyg {{ $errors->has('title') ? 'is-invalid' : '' }}"
                                    >{{ $editNewsItem->content ?? old('content') }}</textarea>
                                    <div class="invalid-feedback">{{ $errors->first('content') }}</div>
                                </div>

                                <p class="text-muted">{{ __('Note: if using links for this website e.g. profile, use full URL.') }}</p>

                                <button class="btn btn-outline-success btn-lg">
                                    @if($editNewsItem)
                                        {{ __('Save and update') }}
                                    @else
                                        {{ __('Save and post') }}
                                    @endif
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card-body">
                            <h3>{{ __('Preview') }}</h3>
                            <hr>
                            <html-preview input-selector="#news-content" html="{{ $editNewsItem->content ?? '' }}"></html-preview>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-sm table-striped">
                        <thead>
                            <tr>
                                <th>{{ __('Title') }}</th>
                                <th>{{ __('Posted by') }}</th>
                                <th>{{ __('Edit') }}</th>
                                <th>{{ __('Delete') }}</th>
                                <th>{{ __('Posted at') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($news as $newsItem)
                            <tr>
                                <td>
                                    <a href="{{ route('home.news', ['id' => $newsItem->id, 'titleSlug' => $newsItem->titleSlug]) }}" target="_blank">
                                        {{ $newsItem->title }}
                                    </a>
                                </td>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <img src="{{ $newsItem->user->avatar }}" class="icon icon-circle pe-n mr-2" alt>
                                        <a href="{{ route('profile', ['userId' => $newsItem->user->id]) }}">
                                            @if($newsItem->user->blocked)
                                            <s class="text-dark">{{ $newsItem->user->username }}</s>
                                            @elseif($newsItem->user->admin)
                                            <span class="text-danger">{{ $newsItem->user->username }}</span>
                                            @else
                                            <span>{{ $newsItem->user->username }}</span>
                                            @endif
                                        </a>
                                    </div>
                                </td>
                                <td>
                                    <a
                                        href="{{ route('admin.news', ['id' => $newsItem->id]) }}"
                                        class="btn btn-link btn-sm"
                                    >
                                        {{ __('Edit') }}
                                    </a>
                                </td>
                                <td>
                                    <form action="{{ route('admin.news.delete') }}" method="post">
                                        @csrf

                                        <input type="hidden" name="id" value="{{ $newsItem->id }}">
                                        <button
                                            class="btn btn-link text-danger btn-sm"
                                            onclick="return confirm('{{ __('Are you sure?') }}')"
                                        >
                                            {{ __('Delete') }}
                                        </button>
                                    </form>
                                </td>
                                <td>
                                    {{ $newsItem->created_at->format('F j, Y H:i') }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
