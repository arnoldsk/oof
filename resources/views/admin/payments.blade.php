@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-sm table-striped">
                        <thead>
                            <tr>
                                <th>{{ __('Username') }}</th>
                                <th>{{ __('Status') }}</th>
                                <th>{{ __('Product') }}</th>
                                <th>{{ __('Price') }}</th>
                                <th>{{ __('Purchased at') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($payments as $payment)
                            <tr v-pre>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <img src="{{ $payment->user->avatar }}" class="icon icon-circle pe-n mr-2" alt>
                                        <a href="{{ route('profile', ['userId' => $payment->user->id]) }}">
                                            @if($payment->user->blocked)
                                            <s class="text-dark">{{ $payment->user->username }}</s>
                                            @elseif($payment->user->admin)
                                            <span class="text-danger">{{ $payment->user->username }}</span>
                                            @else
                                            <span>{{ $payment->user->username }}</span>
                                            @endif
                                        </a>
                                    </div>
                                </td>
                                <td>
                                    <span class="text-{{ $payment->getStatusBootstrapClass() }}">
                                        {{ $payment->getStatusText() }}
                                    </span>
                                </td>
                                <td>
                                    {{ $payment->product->getNameWithPrefix($payment) }}
                                </td>
                                <td>
                                    <span>{{ App\Payment::DEFAULT_CURRENCY }} {{ number_format($payment->amount, 2) }}</span>
                                </td>
                                <td>
                                    {{ date('F j, Y H:i', strtotime($payment->created_at)) }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
