@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <form action="{{ route('admin.settings.save') }}" method="post" autocomplete="off">
                @csrf

                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <h3>{{ __('Path') }}</h3>
                            </div>
                            <div class="col-md-8">
                                <h3>{{ __('Value') }}</h3>
                            </div>
                        </div>

                        @foreach($settings as $setting)
                        <div class="row align-items-center mb-2">
                            <div class="col-md-4 text-right">
                                <h5>{{ $setting->path }}</h5>
                            </div>
                            <div class="col-md-8">
                                <textarea
                                    name="settings[{{ $setting->path }}][value]"
                                    class="form-control"
                                >{{ $setting->value }}</textarea>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="card-footer text-center">
                        <button class="btn btn-outline-success btn-lg">
                            {{ __('Save all settings') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
