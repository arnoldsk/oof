@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-sm table-striped">
                        <thead>
                            <tr>
                                <th>{{ __('Username') }}</th>
                                <th>{{ __('SteamID') }}</th>
                                <th>{{ __('Discord') }}</th>
                                <th>{{ __('Payments') }}</th>
                                <th>{{ __('Donations') }}</th>
                                <th>{{ __('Joined at') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <img src="{{ $user->avatar }}" class="icon icon-circle pe-n mr-2" alt>
                                        <a href="{{ route('profile', ['userId' => $user->id]) }}">
                                            @if($user->blocked)
                                            <s class="text-dark">{{ $user->username }}</s>
                                            @elseif($user->admin)
                                            <span class="text-danger">{{ $user->username }}</span>
                                            @else
                                            {{ $user->username }}
                                            @endif
                                        </a>
                                    </div>
                                </td>
                                <td>
                                    <a href="{{ $user->profileurl }}" target="_blank">
                                        {{ $user->steamid }}
                                    </a>
                                </td>
                                <td>
                                    @if($discordUser = $user->discord)
                                    <div class="d-flex align-items-center">
                                        <img src="{{ $discordUser->avatar }}?size=32" class="icon icon-circle pe-n mr-2" alt>
                                        <span>{{ $discordUser->username }}#{{ $discordUser->discriminator }}</span>
                                    </div>
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    @if($paymentCount = $user->payments->count())
                                    <a href="{{ route('admin.payments', ['userId' => $user->id]) }}">
                                        {{ __('View :count', ['count' => $paymentCount]) }}
                                    </a>
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    @if($donationCount = $user->donations->count())
                                    <a href="{{ route('admin.donations', ['userId' => $user->id]) }}">
                                        {{ __('View :count', ['count' => $donationCount]) }}
                                    </a>
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    @if($user->created_at)
                                    {{ $user->created_at->format('F j, Y H:i') }}
                                    @else
                                    -
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
