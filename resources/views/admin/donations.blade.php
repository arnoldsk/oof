@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('View Donations') }}</div>

                <div class="card-body">
                    <p>{{ __('We are not able to confirm that the user has truly completed the donation so take the "completed" status with a grain of salt.') }}</p>

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <td>{{ __('Username') }}</td>
                                <td>{{ __('Status') }}</td>
                                <td>{{ __('Started at') }}</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($donations as $donation)
                            <tr>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <img src="{{ $donation->user->avatar }}" class="icon icon-circle pe-n mr-2" alt>
                                        <a href="{{ route('profile', ['userId' => $donation->user->id]) }}">
                                            @if($donation->user->blocked)
                                            <s class="text-dark">{{ $donation->user->username }}</s>
                                            @elseif($donation->user->admin)
                                            <span class="text-danger">{{ $donation->user->username }}</span>
                                            @else
                                            <span>{{ $donation->user->username }}</span>
                                            @endif
                                        </a>
                                    </div>
                                </td>
                                <td>
                                    <span class="text-{{ $donation->getStatusBootstrapClass() }}">
                                        {{ $donation->getStatusText() }}
                                    </span>
                                </td>
                                <td>
                                    {{ date('F j, Y H:i', strtotime($donation->created_at)) }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
