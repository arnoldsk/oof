@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body text-center">
                    <p>{{ __('You will be redirected to Steam to finish authentication.') }}</p>

                    <a href="{{ route('auth.steam') }}">
                        <img src="https://steamcommunity-a.akamaihd.net/public/images/signinthroughsteam/sits_01.png">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
