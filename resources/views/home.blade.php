@extends('layouts.app')

@section('content')
<div class="container">
    <div class="jumbotron text-center" style="display: none;" ref="homeJumbotron">
        <h1 class="display-3">Big oof!</h1>
        <p class="lead">We are not sure ourselves what exactly is this.</p>
        <p><small>(Allan please add description)</small></p>
        <p class="lead">
            <button class="btn btn-outline-primary btn-lg" @click="acceptHomeJumbotron">
                {{ __('Continue') }}
            </button>
            <a class="btn btn-outline-info btn-lg" href="{{ route('discord') }}" target="_blank">
                {{ __('Join Discord') }}
            </a>
        </p>
    </div>

    <div class="row justify-content-around">
        <div class="col-md-8">
            @foreach($news as $newsItem)
            <div class="card mb-4">
                <h3 class="card-header">
                    <a href="{{ route('home.news', ['id' => $newsItem->id, 'titleSlug' => $newsItem->titleSlug]) }}">
                        {{ $newsItem->title }}
                    </a>
                </h3>

                <div class="card-body">
                    <html-preview html="{{ $newsItem->content }}"></html-preview>
                </div>

                <div class="card-footer">
                    <div class="d-flex align-items-center">
                        <img src="{{ $newsItem->user->avatar }}" class="icon icon-circle pe-n mr-2" alt>
                        <a href="{{ route('profile', ['userId' => $newsItem->user->id]) }}">
                            @if($newsItem->user->blocked)
                            <s class="text-dark">{{ $newsItem->user->username }}</s>
                            @elseif($newsItem->user->admin)
                            <span class="text-danger">{{ $newsItem->user->username }}</span>
                            @else
                            <span>{{ $newsItem->user->username }}</span>
                            @endif
                        </a>
                        <span>&nbsp;</span>
                        {{ __('at :date', ['date' => date('F j, Y H:i', strtotime($newsItem->created_at))]) }}</span>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

        @if(!$single)
        <div class="col-md-4">
            <game-server-cards
                :servers="{{ json_encode($servers) }}"
                server-data-url="{{ route('home') }}"
                no-map-image-url="{{ asset('img/no-map-image.png') }}"
            ></game-server-cards>
        </div>
        @endif
    </div>
</div>
@endsection
