@extends('layouts.app')

@section('content')
<div class="container">
    <paypal-shop
        :is-logged-in="{{ \Auth::id() ? 'true' : 'false' }}"
        :products="{{ $products }}"
        currency="{{ App\Payment::DEFAULT_CURRENCY }}"
        :paypal-options="{{ json_encode($paypalOptions) }}"
        get-products-url="{{ route('shop') }}"
        :goal-percentage="{{ $goalPercentage }}"
        :tier-perks="{{ json_encode($tierPerks) }}"
    ></paypal-shop>
</div>
@endsection
