
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context('./components/', true, /\.vue$/i);

files.keys().map((key) => Vue.component(key.split('/').slice(1).join('').split('.')[0], files(key).default));

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',

    methods: {
        elTriggerEvent(element, event) {
            if ('createEvent' in document) {
                const event = document.createEvent('HTMLEvents');

                event.initEvent('change', false, true);
                element.dispatchEvent(event);
            } else {
                element.fireEvent('onchange');
            }
        },

        initWysiwygEditors() {
            tinymce.init({
                selector: '.wysiwyg',
                height: 300,
                plugins: 'advcode, formatpainter, link, linkchecker, media mediaembed, powerpaste, tinymcespellchecker',
                init_instance_callback: (editor) => {
                    editor.on('KeyUp', (e) => {
                        const inputElm = editor.targetElm;

                        inputElm.value = editor.getContent();

                        this.elTriggerEvent(inputElm, 'change');
                    });
                }
            });
        },

        onPaypalDonateStart() {
            axios({
                url: `${APP_URL}/donate/start`,
                method: 'post',
            });
        },

        formatPrice(value) {
            let val = (value / 1).toFixed(2).replace('.', ',');

            return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
        },

        initJumbotron() {
            const el = this.$refs.homeJumbotron;

            if (document.cookie.indexOf('jumbotron-accepted.tmp') === -1) {
                $(el).slideDown(300);
            }
        },

        acceptHomeJumbotron() {
            const el = this.$refs.homeJumbotron;

            $(el).slideUp(300);

            document.cookie = 'jumbotron-accepted.tmp=1; path=/; expires=Fri, 12 Sep 275760 18:10:24 GMT;';
        },
    },

    mounted() {
        this.initJumbotron();
        this.initWysiwygEditors();
    },
});
