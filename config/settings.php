<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Settings Values
    |--------------------------------------------------------------------------
    |
    | Mapped by path - value.
    | All paths that are not specified here will be returned as NULL.
    |
    */

    'shop_goal' => 0,

];
