<?php

return [

    'mode' => env('PAYPAL_MODE', 'sandbox'),

    'donate_button_id' => env('PAYPAL_DONATE_BUTTON_ID', ''),

    'app' => [
        'client_id' => env('PAYPAL_APP_CLIENT_ID', ''),

        'secret' => env('PAYPAL_APP_SECRET', ''),
    ],

];
