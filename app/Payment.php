<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    const DEFAULT_CURRENCY = 'EUR';

    const STATUS_REFUNDED = 0;
    const STATUS_COMPLETED = 1;

    protected $fillable = [
        'user_id',
        'product_id',
        'payment_id',
        'payer_id',
        'status',
        'amount',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function getAmountAttribute($amount)
    {
        return $amount / 100;
    }

    public function setAmountAttribute($amount)
    {
        $this->attributes['amount'] = $amount * 100;
    }

    public function getStatusText()
    {
        return [
            self::STATUS_REFUNDED => __('Refunded'),
            self::STATUS_COMPLETED => __('Completed'),
        ][$this->status];
    }

    public function getStatusBootstrapClass()
    {
        return [
            self::STATUS_REFUNDED => 'danger',
            self::STATUS_COMPLETED => 'success',
        ][$this->status];
    }

    public static function getTotalAmountThisMonth()
    {
        $totalAmount = 0;

        $payments = self::whereMonth('created_at', \Carbon::now()->month);

        return (int)$payments->sum('amount') / 100;
    }
}
