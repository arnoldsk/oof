<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class RemoveDemoFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:remove-demo-files';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Removes old demo files from public/dem/';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $pubFileDir = public_path('dem');

        if (!is_dir($pubFileDir)) {
            return;
        }

        // Carbon was messing it up, using the old method
        $dateNow = new \DateTime();
        $deleted = 0;

        foreach (glob(sprintf('%s/*.dem', $pubFileDir)) as $demFilePath) {
            $timeModified = filemtime($demFilePath);
            $dateModified = new \DateTime(sprintf('@%s', $timeModified));

            $deleted++;
            unlink($demFilePath);
        }

        if ($deleted) {
            echo sprintf('Deleted %s file(s)', $deleted) . PHP_EOL;
        } else {
            echo 'Nothing to delete' . PHP_EOL;
        }
    }
}
