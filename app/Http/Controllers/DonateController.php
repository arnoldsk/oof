<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Donation;

class DonateController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('donate', [
            'title' => __('Support by donating'),
            'paypalButtonId' => app()->config->get('paypal.donate_button_id'),
        ]);
    }

    /**
     * Creates an initial donation item for the user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function start()
    {
        $response = [
            'success' => false,
        ];
        $userId = \Auth::id();

        if ($userId) {
            try {
                $donation = Donation::where([
                    'user_id' => $userId,
                    'status' => Donation::STATUS_PENDING,
                ])->first();

                if (!$donation) {
                    Donation::create([
                        'user_id' => $userId,
                        'status' => Donation::STATUS_PENDING,
                    ]);
                }

                $response['success'] = true;
                $response['message'] = __('Started a new donation.');
            } catch (Exception $e) {
                $response['message'] = __('Count not start a donation.');
            }
        } else {
            $response['message'] = __('Not logged in');
        }

        return response()->json($response, $response['success'] ? 200 : 400);
    }

    public function paypalCompleted()
    {
        $response = [
            'success' => __('Thank you for donating!'),
        ];

        // Set the status to approval if there is a pending donation
        if ($userId = \Auth::id()) {
            $donation = Donation::where([
                'user_id' => $userId,
                'status' => Donation::STATUS_PENDING,
            ])->first();

            if ($donation) {
                $donation->status = Donation::STATUS_COMPLETED;
                $donation->save();
            } else {
                // User most likely landed here via the URL
                // Redirect to the main page with no messages
                $response = [];
            }
        }

        return redirect()->route('donate')->with($response);
    }

    public function paypalCancel()
    {
        $response = [
            'success' => __('Donation was canceled.'),
        ];

        if ($userId = \Auth::id()) {
            // Set the status to approval if there is a pending donation
            $donation = Donation::where([
                'user_id' => $userId,
                'status' => Donation::STATUS_PENDING,
            ])->first();

            if ($donation) {
                $donation->status = Donation::STATUS_CANCELED;
                $donation->save();
            } else {
                // User most likely landed here via the URL
                // Redirect to the main page with no messages
                $response = [];
            }
        }

        return redirect()->route('donate')->with($response);
    }
}
