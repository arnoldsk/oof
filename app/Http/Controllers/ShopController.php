<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;
use App\PayPal;
use App\Product;
use App\Setting;

class ShopController extends Controller
{
    /**
     * @var PayPal;
     */
    protected $paypal;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PayPal $paypal)
    {
        $this->middleware('auth')->except(['index']);

        $this->paypal = $paypal;
    }

    /**
     * Show the application dashboard.
     *
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        // Get goal percentage value
        if ($goalMax = Setting::getValue('shop_goal')) {
            $paymentMonthAmount = Payment::getTotalAmountThisMonth();
            $goalPercentage = $paymentMonthAmount * 100 / $goalMax;
            $goalPercentage = $goalPercentage <= 100 ? $goalPercentage : 100;
        } else {
            $goalPercentage = 0;
        }

        // Get products and add supporter prefixes (should make this as a builder extension)
        $products = Product::orderBy('price', 'asc')->get();
        foreach ($products as $product) {
            $product->name = $product->getNameWithPrefix();
        }

        // Get tier perk descriptions
        $tierPerks = [];
        $perkSettings = Setting::where('path', 'like', 'product_tier_%')->get();

        foreach ($perkSettings as $setting) {
            preg_match('/product_tier_([0-9]+)_perks/', $setting->path, $matches);
            $tier = (int)$matches[1];

            $tierPerks['Tier ' . $tier] = explode(PHP_EOL, $setting->value);
        }

        // Sort the tiers high to low
        krsort($tierPerks);

        $data = [
            'title' => __('Support us with packs'),
            'goalPercentage' => $goalPercentage,
            'products' => $products,
            'paypalOptions' => PayPal::getOptions(),
            'tierPerks' => $tierPerks,
        ];

        if ($request->expectsJson() && $request->ajax) {
            return response()->json(json_encode($data));
        }

        return view('shop', $data);
    }

    /**
     * Create the paypal payment
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function paymentCreate(Request $request)
    {
        $response = [
            'success' => false,
        ];

        $productId = (int)$request->productId;
        $product = Product::find($productId);

        if ($product) {
            // Check if customer already has this product
            $userHasPurchased = $product->hasUserPurchased(\Auth::user());

            if (!$userHasPurchased) {
                $productData = array(
                    'name' => $product->getNameWithPrefix(),
                    'sku' => $product->id,
                    'price' => $product->finalPrice,
                );
                $payment = $this->paypal->createPayment($productData, route('shop'), route('shop'));

                if ($payment) {
                    $response['success'] = true;
                    $response['message'] = __('Created a new payment.');
                    $response['id'] = $payment->id;
                } else {
                    $response['message'] = __('Could not create the payment.');
                }
            } else {
                $resposne['message'] = __('User already owns this product.');
            }
        } else {
            $response['message'] = __('Product was not found.');
        }

        return response()->json($response, 200);
    }

    /**
     * Execute the paypal payment
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function paymentExecute(Request $request)
    {
        $response = [
            'success' => false,
        ];

        $paymentId = $request->paymentId;
        $payerId = $request->payerId;

        if ($paymentId && $payerId) {
            $payment = $this->paypal->executePayment($paymentId, $payerId);
            $item = $this->paypal->getPaymentFirstItem($payment);

            if ($payment && !empty($item)) {
                // Create a payment entry
                Payment::create([
                    'user_id' => \Auth::id(),
                    'product_id' => $item->getSku(),
                    'payment_id' => $paymentId,
                    'payer_id' => $payerId,
                    'status' => Payment::STATUS_COMPLETED,
                    'amount' => $item->getPrice(),
                ]);

                $response['success'] = true;
                $response['message'] = __('Payment successful.');
            } else {
                $response['message'] = __('Could not finish the payment.');
            }
        } else {
            $response['message'] = __('Invalid payment data.');
        }

        return response()->json($response, 200);
    }
}
