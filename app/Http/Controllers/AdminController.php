<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\News;
use App\Payment;
use App\Donation;
use App\Setting;
use App\Product;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /**
     * Manage users
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function users()
    {
        return view('admin.users', [
            'title' => __('Users list'),
            'users' => User::orderBy('username')->get(),
        ]);
    }

    /**
     * Manage user payments
     *
     * @param int|null $userId
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function payments($userId = null)
    {
        if ($userId) {
            $payments = Payment::where(['user_id' => $userId])->orderByDesc('updated_at')->get();
        } else {
            $payments = Payment::orderByDesc('created_at')->get();
        }

        return view('admin.payments', [
            'title' => __('Payments list'),
            'payments' => $payments,
        ]);
    }

    /**
     * Manage users
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function donations($userId = null)
    {
        if ($userId) {
            $donations = Donation::where(['user_id' => $userId])->orderByDesc('updated_at')->get();
        } else {
            $donations = Donation::orderByDesc('updated_at')->get();
        }

        return view('admin.donations', [
            'title' => __('Donations list'),
            'donations' => $donations,
        ]);
    }

    /**
     * Manage news
     *
     * @param int $id
     * @return mixed
     */
    public function news($id = 0)
    {
        $editNewsItem = News::find($id);

        if ($id > 0 && !$editNewsItem) {
            return redirect()->route('admin.news')
                ->with('error', __('Could not find the news item.'));
        }

        return view('admin.news', [
            'title' => __('News management'),
            'editNewsItem' => $editNewsItem,
            'news' => News::orderByDesc('created_at')->get(),
        ]);
    }

    /**
     * Create or update a news post
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function newsSave(Request $request)
    {
        $this->validateNewsData($request);

        $id = $request->post('id');
        $newsItem = News::findOrNew($id);

        if ($id > 0 && !$newsItem->id) {
            return back()->with('error', __('Could not find the news item.'));
        }

        if (!$newsItem->id) {
            $newsItem->user_id = \Auth::id();
        }

        $newsItem->title = $request->post('title');
        $newsItem->content = $request->post('content');
        $newsItem->save();

        if (!$newsItem->id) {
            return back()->with('success', __('News post created.'));
        } else {
            return redirect()->route('admin.news')
                ->with('success', __('News post updated.'));
        }
    }

    /**
     * Delete a news post
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function newsDelete(Request $request)
    {
        $id = $request->post('id');
        $newsItem = News::find($id);

        if (!$newsItem) {
            return back()->with('error', __('Could not find the news item.'));
        }

        $newsItem->delete();

        return redirect()->route('admin.news')
            ->with('success', __('News post deleted.'));
    }

    /**
     * Validate the news request data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateNewsData(Request $request)
    {
        $request->validate([
            'title' => 'required|string',
            'content' => 'required|string',
        ]);
    }

    /**
     * Manage settings
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function settings()
    {
        return view('admin.settings', [
            'title' => __('Manage settings'),
            'settings' => Setting::orderBy('path')->get(),
        ]);
    }

    /**
     * Save settings
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function settingsSave(Request $request)
    {
        $settingsData = $request->post('settings');

        foreach ($settingsData as $path => $data) {
            $setting = Setting::firstOrNew(['path' => $path]);
            $setting->value = trim($data['value']);
            $setting->save();
        }

        return back()->with('success', __('Settings updated.'));
    }
}
