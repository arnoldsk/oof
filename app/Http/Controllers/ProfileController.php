<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Discord;

class ProfileController extends Controller
{
    protected $apiUrl = 'https://discordapp.com/api';

    /**
     * Show the landing page.
     *
     * @param int $userId
     * @return mixed
     */
    public function index($userId = 0)
    {
        if ($userId > 0) {
            $user = User::find($userId);
        } else {
            $user = \Auth::user();
        }

        if (!$user) {
            abort(404); // TODO redirect to /profiles with error
        }

        $data = [
            'title' => __(':username\'s Profile', ['username' => $user->username]),
            'user' => $user,
        ];

        return view('profile', $data);
    }

    /**
     * Redirect the user to authorize with discord
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function discordUpdate()
    {
        $this->middleware('auth');

        $path = '/oauth2/authorize';
        $params = [
            'client_id' => app()->config->get('discord.client_id'),
            'redirect_uri' => route('profile.discord.return'),
            'response_type' => 'code',
            'scope' => 'identify',
        ];
        $url = sprintf('%s%s?%s', $this->apiUrl, $path, http_build_query($params));

        return redirect()->to($url);
    }

    /**
     * Handle returned data from discord
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function discordReturn(Request $request)
    {
        $this->middleware('auth');
        $response = [];

        // Fetch the authotization token
        if ($code = $request->code) {
            $path = '/oauth2/token';
            $params = [
                'client_id' => app()->config->get('discord.client_id'),
                'client_secret' => app()->config->get('discord.client_secret'),
                'grant_type' => 'authorization_code',
                'code' => $code,
                'redirect_uri' => route('profile.discord.return'),
                'scope' => 'identify',
            ];
            $url = sprintf('%s%s', $this->apiUrl, $path);

            $ch = curl_init();
            curl_setopt_array($ch, [
                CURLOPT_URL => $url,
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS => http_build_query($params),
                CURLOPT_RETURNTRANSFER => 1,
            ]);
            $token = json_decode(curl_exec($ch));
            curl_close($ch);

            // Fetch user information
            if (isset($token->access_token)) {
                $path = '/users/@me';
                $url = sprintf('%s%s', $this->apiUrl, $path);

                $ch = curl_init();
                curl_setopt_array($ch, [
                    CURLOPT_URL => $url,
                    CURLOPT_HTTPHEADER => array(
                        sprintf('Authorization: Bearer %s', $token->access_token),
                    ),
                    CURLOPT_RETURNTRANSFER => 1,
                ]);
                $user = json_decode(curl_exec($ch));
                curl_close($ch);

                // Create or update the Discord entry
                $discord = Discord::firstOrNew(['discord_user_id' => $user->id]);
                $discord->user_id = \Auth::id();
                $discord->discord_user_id = $user->id;
                $discord->username = $user->username;
                $discord->discriminator = $user->discriminator;
                $discord->avatar = sprintf('https://cdn.discordapp.com/avatars/%s/%s.png', $user->id, $user->avatar);
                $discord->save();

                $response['success'] = __('Successfully updated Discord information.');
            } else {
                $response['error'] = __('Could not authenticate the Discord user.');
            }
        }

        return redirect()->route('profile')->with($response);
    }
}
