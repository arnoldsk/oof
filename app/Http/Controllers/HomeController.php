<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\User;
use App\GameServers;
use App\GameServerFtp;

class HomeController extends Controller
{
    /**
     * @var GameServers
     */
    protected $gameServers;

    /**
     * @var GameServerFtp
     */
    protected $gameServerFtp;

    /**
     * @param GameServers $gameServers
     */
    public function __construct(GameServers $gameServers, GameServerFtp $gameServerFtp)
    {
        $this->gameServers = $gameServers;
        $this->gameServerFtp = $gameServerFtp;
    }

    /**
     * Show the landing page.
     *
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $data = [
            'news' => News::orderByDesc('created_at')->limit(10)->get(),
            'single' => false,
            'servers' => $this->gameServers->getData(),
        ];

        if ($request->expectsJson() && $request->ajax) {
            return response()->json($data);
        }

        return view('home', $data);
    }

    /**
     *
     * @param int $id
     * @param string $slug
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function news($id, $slug, Request $request)
    {
        $news = News::where('id', $id)->limit(1)->get();
        $newsItem = $news->first();

        // Using foreach on 1 item just for convenience - validates itself
        if (!$news->count() || $newsItem->titleSlug !== $slug) {
            return redirect(route('home'))->with([
                'error' => __('News post was not found.'),
            ]);
        }

        $data = [
            'title' => $newsItem->title,
            'news' => $news,
            'single' => $id > 0,
            'servers' => $this->gameServers->getData(),
        ];

        if ($request->expectsJson() && $request->ajax) {
            return response()->json($data);
        }

        return view('home', $data);
    }

    /**
     * Display all users
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function users()
    {
        return view('users', [
            'users' => User::orderBy('username')->get(),
        ]);
    }

    /**
     * Redirects to Discord general channel
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function discord(Request $request)
    {
        return redirect()->to(app()->config->get('discord.invite_url'));
    }

    /**
     * Redirects to Discord support channel
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function support(Request $request)
    {
        return redirect()->to(app()->config->get('discord.invite_url'));
    }

    /**
     * Redirects to Discord support channel
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function steamgroup(Request $request)
    {
        return redirect()->to('https://steamcommunity.com/groups/ooflv');
    }

    /**
     * List or download demos
     *
     * @param string|null $filename
     * @return mixed
     */
    public function demos($filename = null)
    {
        $demoFiles = $this->gameServerFtp->getDemoFiles();

        $filenames = array_map(function($file) {
            return basename($file);
        }, $demoFiles);
        $filenames = array_slice($filenames, 0, 20);

        if ($filename) {
            $demoFile = $this->gameServerFtp->getDownloadedDemoFile($filename);

            if ($demoFile) {
                $demoFileUrl = url('dem' . '/' . basename($demoFile));

                return redirect()->to($demoFileUrl);
            } else {
                return back()->with('error', 'Could not download the demo file.');
            }
        }

        return view('demos', [
            'title' => __('Demo list'),
            'filenames' => $filenames,
        ]);
    }
}
