<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Invisnik\LaravelSteamAuth\SteamAuth;
use App\User;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    /**
     * The SteamAuth instance.
     *
     * @var SteamAuth
     */
    protected $steam;

    /**
     * The redirect URL.
     *
     * @var string
     */
    protected $redirectPath = '/';

    /**
     * AuthController constructor.
     *
     * @param SteamAuth $steam
     */
    public function __construct(SteamAuth $steam)
    {
        $this->steam = $steam;
    }

    /**
     * Authentication page to prevent instant redirection
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('auth');
    }

    /**
     * Redirect the user to the authentication page
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function redirectToSteam()
    {
        return $this->steam->redirect();
    }

    /**
     * Get user info and log in
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function handle()
    {
        if ($this->steam->validate()) {
            $info = $this->steam->getUserInfo();

            if (!is_null($info)) {
                $user = $this->findOrNewUser($info);

                \Auth::login($user, true);

                $redirectPath = session('steam_referrer', $this->redirectPath);

                return redirect($redirectPath);
            }
        }

        return $this->redirectToSteam();
    }

    /**
     * Getting user by info or created if not exists
     *
     * @param $info
     * @return User
     */
    protected function findOrNewUser($info)
    {
        $user = User::where('steamid', $info->steamID64)->first();

        if (!is_null($user)) {
            // Update data that can change
            $user->update([
                'username' => $info->personaname,
                'avatar' => $info->avatarfull,
            ]);

            return $user;
        }

        return User::create([
            'steamid' => $info->steamID64,
            'steamid32' => User::steamId64to32($info->steamID64),
            'username' => $info->personaname,
            'avatar' => $info->avatarfull,
            'profileurl' => $info->profileurl,
        ]);
    }
}
