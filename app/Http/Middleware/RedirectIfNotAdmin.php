<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfNotAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (!Auth::guard($guard)->check()) {
            return redirect()->route('auth')
                ->with(['error' => __('You need to be a guest to view this page.')]);
        }

        if (!Auth::user()->admin) {
            return redirect()->route('home')
                ->with(['error' => __('You need admin access to view this page.')]);
        }

        return $next($request);
    }
}
