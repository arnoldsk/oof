<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        if (!$request->expectsJson()) {
            session([
                'steam_referrer' => '/' . $request->path(),
            ]);

            $request->session()->flash('error', __('You need to be logged in to view this page.'));

            return route('auth');
        }
    }
}
