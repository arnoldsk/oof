<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = [
        'path',
        'value',
    ];

    public function getValueAttribute($value)
    {
        $pathRaw = $this->getOriginal('path');

        return $value ?: self::getDefaultValue($pathRaw);
    }

    public static function getValue($path)
    {
        $setting = self::where('path', $path)->get()->first();

        return $setting ? $setting->value : self::getDefaultValue($path);
    }

    public static function getDefaultValue($path)
    {
        $key = sprintf('settings.%s', $path);

        return app()->config->get($key);
    }
}
