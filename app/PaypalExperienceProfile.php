<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaypalExperienceProfile extends Model
{
    protected $table = 'paypal_experience_profile';
    
    protected $fillable = [
        'profile_id',
    ];
}
