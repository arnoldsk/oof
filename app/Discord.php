<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discord extends Model
{
    protected $fillable = [
        'user_id',
        'discord_user_id',
        'username',
        'discriminator',
        'avatar',
    ];

    public function user()
    {
        return $this->hasOne('App\User');
    }
}
