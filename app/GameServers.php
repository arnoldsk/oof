<?php

namespace App;

class GameServers
{
    /**
     * @var string
     */
    protected $url = '';

    /**
     * @var array
     */
    protected $dataFields = [
        'host',
        'players',
        'map',
    ];

    public function __construct()
    {
        $this->url = app()->config->get('app.sourcebans_url');

        // SB requires slash due to a scuffed htaccess setup
        $this->url = trim($this->url, '/') . '/';
    }

    /**
     * @return int
     */
    public function getServerCount()
    {
        return 1; // TODO: dynamic
    }

    /**
     * @return array
     */
    public function getData()
    {
        $result = [];

        foreach (range(0, $this->getServerCount() - 1) as $serverIndex) {
            $requestData = [
                'xajax' => 'ServerHostPlayers',
                'xajaxr' => 1548849355217,
                'xajaxargs' => [
                    ($serverIndex + 1), 'servers', '', $serverIndex, -1, 1, 70,
                ],
            ];
            $response = $this->getResponse($requestData);
            $data = $this->parseResponse($response, $serverIndex);

            $result[] = $data;
        }

        return $result;
    }

    protected function getResponse($data)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    /**
     * @param string $resposne
     * @return array
     */
    protected function parseResponse($response, $serverIndex)
    {
        $data = [];
        $typeFormat = '%s_%s';

        libxml_use_internal_errors(true);

        if ($sxml = simplexml_load_string($response)) {
            foreach ($sxml->cmd as $cmd) {
                $cmdType = (string)$cmd['t'];
    
                foreach ($this->dataFields as $field) {
                    $fieldAsType = sprintf($typeFormat, $field, $serverIndex + 1);
    
                    if ($fieldAsType === $cmdType) {
                        $data[$field] = (string)$cmd;
                        continue;
                    }
                }
            }
    
            // Apply images if found
            if (isset($data['map'])) {
                $imageUrl = sprintf('%s/images/maps/%s.jpg', $this->url, $data['map']);
                $imageExists = @file_get_contents($imageUrl);
    
                if ($imageExists) {
                    $data['map_image'] = $imageUrl ?: null;
                }
            }
        }

        return $data;
    }
}
