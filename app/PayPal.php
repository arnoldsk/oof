<?php

namespace App;

use \App\PaypalExperienceProfile;
use \PayPal\Rest\ApiContext;
use \PayPal\Auth\OAuthTokenCredential;
use \PayPal\Api\Payer;
use \PayPal\Api\Item;
use \PayPal\Api\ItemList;
use \PayPal\Api\Amount;
use \PayPal\Api\Transaction;
use \PayPal\Api\RedirectUrls;
use \PayPal\Api\Payment;
use \PayPal\Api\PaymentExecution;
use \PayPal\Api\FlowConfig;
use \PayPal\Api\Presentation;
use \PayPal\Api\InputFields;
use \PayPal\Api\WebProfile;
use \PayPal\Api\CreateProfileResponse;
use \PayPal\Exception\PayPalConnectionException;

class PayPal
{
    /**
     * @var string
     */
    protected $currency = '';

    /**
     * @var array
     */
    protected $errors = [];

    public function __construct() {
        $this->currency = \App\Payment::DEFAULT_CURRENCY;
    }

    /**
     * Three letter currency code
     * 
     * @param string $currency
     * @return this
     */
    public function setCurrency($currency)
    {
        if (mb_strlen($currency) !== 3) {
            throw new Exception('Currency has to be a 3 character string.');
        }

        $this->currency = strtoupper($currency);
        
        return $this;
    }

    /**
     * Returns options array for front-end use
     * 
     * @return array
     */
    public static function getOptions()
    {
        $paypalMode = app()->config->get('paypal.mode');
        $clientId = app()->config->get('paypal.app.client_id');

        return [
            'mode' => $paypalMode,
            'clientId' => $clientId,
            'createPaymentUrl' => route('shop.payment.create'),
            'executePaymentUrl' => route('shop.payment.execute'),
        ];
    }

    /**
     * Create an API context with credentials
     * 
     * @return ApiContext
     */
    protected function getApiContext()
    {
        $clientId = app()->config->get('paypal.app.client_id');
        $secret = app()->config->get('paypal.app.secret');

        $apiContext = new ApiContext(
            new OAuthTokenCredential($clientId, $secret)
        );

        $apiContext->setConfig([
            'mode' => app()->config->get('paypal.mode'),
        ]);

        return $apiContext;
    }

    /**
     * Creates a new payment
     * 
     * @param array $productData
     * @return Payment|false
     */
    public function createPayment($productData, $returnUrl, $cancelUrl)
    {
        $apiContext = $this->getApiContext();

        // Create a payment
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $item = new Item();
        $item->setName($productData['name'] ?? 'Unknown Product');
        $item->setCurrency($this->currency);
        $item->setQuantity($productData['quantity'] ?? 1);
        $item->setSku($productData['sku'] ?? 'unknown-sku');
        $item->setPrice($productData['price'] ?? 0);

        $itemList = new ItemList();
        $itemList->setItems([$item]);

        $amount = new Amount();
        $amount->setTotal($productData['price']);
        $amount->setCurrency($this->currency);

        $transaction = new Transaction();
        $transaction->setAmount($amount);
        $transaction->setItemList($itemList);

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl($returnUrl);
        $redirectUrls->setCancelUrl($cancelUrl);

        $payment = new Payment();
        $payment->setIntent('sale');
        $payment->setPayer($payer);
        $payment->setTransactions(array($transaction));
        $payment->setRedirectUrls($redirectUrls);

        if ($experienceProfileId = $this->getExperienceProfileId()) {
            $payment->setExperienceProfileId($experienceProfileId);
        }

        try {
            $payment->create($apiContext);
        } catch (PayPalConnectionException $e) {
            $this->handleException($e);

            return false;
        }

        return $payment;
    }

    /**
     * Get the first product item from payment data
     * 
     * @param \PayPal\Api\Payment
     * @return \PayPal\Api\Item|null
     */
    public function getPaymentFirstItem(Payment $payment)
    {
        $transactions = $payment->getTransactions();

        if (!empty($transactions)) {
            /** @var \PayPal\Api\ItemsList $itemsList */
            $itemsList = $transactions[0]->getItemList();
            $items = $itemsList->getItems();

            if (!empty($items)) {
                return $items[0];
            }
        }
        
        return null;
    }

    /**
     * Executes a created payment
     * 
     * @param string $payerId
     * @param string $payerId
     * @return Payment|boolean
     */
    public function executePayment($paymentId, $payerId)
    {
        $apiContext = $this->getApiContext();

        // Fetch the payment and initiate execution for the payer
        $payment = Payment::get($paymentId, $apiContext);
        $execution = new PaymentExecution();
        $execution->setPayerId($payerId);

        try {
            // Executes the payment
            $payment->execute($execution, $apiContext);

            // Fetch the payment again to get the latest transaction data
            $payment = Payment::get($paymentId, $apiContext);
        } catch (Exception $e) {
            $this->handleException($e);

            return false;
        };

        return $payment;
    }

    /**
     * Checks if payment profile is already saved and creates a permanent profile if it doesn't
     * 
     * @return string
     */
    public function getExperienceProfileId()
    {
        $savedProfile = PaypalExperienceProfile::first();

        if (!$savedProfile) {
            // Create a permanent account
            $profile = $this->createExperienceProfile(false);

            if ($profile) {
                $savedProfile = PaypalExperienceProfile::create([
                    'profile_id' => $profile->getId(),
                ]);
            }
        }

        return $savedProfile->profile_id;
    }

    /**
     * Create and return a payment experience profile
     * 
     * @param boolean $temporary
     * @return CreateProfileResponse|null
     */
    public function createExperienceProfile($temporary = true)
    {
        $apiContext = $this->getApiContext();

        $flowConfig = new FlowConfig();
        $flowConfig->setLandingPageType('Billing'); // Billing or Login
        $flowConfig->setBankTxnPendingUrl(url('/')); // Not sure what this is
        $flowConfig->setUserAction('commit'); // Instant payment

        $presentation = new Presentation();
        $presentation->setLogoImage(asset('favicon.png'));
        $presentation->setLocaleCode('LV');
        $presentation->setReturnUrlLabel(__('Return'));
        $presentation->setNoteToSellerLabel(__('Thank you!'));

        $inputFields = new InputFields();
        $inputFields->setAllowNote(true);
        $inputFields->setNoShipping(1);
        $inputFields->setAddressOverride(0);

        $webProfile = new WebProfile();
        $webProfile->setName('OOF.LV' . uniqid());
        $webProfile->setFlowConfig($flowConfig);
        $webProfile->setPresentation($presentation);
        $webProfile->setInputFields($inputFields);
        $webProfile->setTemporary($temporary);

        try {
            $createProfileResponse = $webProfile->create($apiContext);
        } catch (PayPalConnectionException $e) {
            $this->handleException($e);

            return null;
        }

        return $createProfileResponse;
    }

    /**
     * Logs and stores the error message
     * 
     * @param \PayPal\Exception\PayPalConnectionException|\Exception $e
     * @return void
     */
    protected function handleException($e)
    {
        $message = $e->getMessage();

        $this->errors[] = $message;
        \Log::error($message);
    }

    /**
     * Returns error array
     * 
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }
}
