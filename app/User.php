<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'steamid',
        'steamid32',
        'username',
        'profileurl',
        'avatar',
        'admin',
        'blocked',
    ];

    public function getAdminAttribute($admin)
    {
        return (bool)$admin;
    }

    public function setAdminAttribute($admin)
    {
        $this->attributes['admin'] = (int)$admin;
    }

    public function getBlockedAttribute($blocked)
    {
        return (bool)$blocked;
    }

    public function setBlockedAttribute($blocked)
    {
        $this->attributes['blocked'] = (int)$blocked;
    }

    public function discord()
    {
        return $this->hasOne('App\Discord');
    }

    public function news()
    {
        return $this->hasMany('App\News');
    }

    public function payments()
    {
        return $this->hasMany('App\Payment');
    }

    public function donations()
    {
        return $this->hasMany('App\Donation');
    }

    public static function steamId64to32($id)
    {
        if (preg_match('/^STEAM_/', $id)) {
            $split = explode(':', $id);

            return $split[2] * 2 + $split[1];
        } elseif (preg_match('/^765/', $id) && strlen($id) > 15) {
            return bcsub($id, '76561197960265728');
        }

        return $id;
    }
}
