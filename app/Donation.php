<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{
    const STATUS_CANCELED = 0;
    const STATUS_PENDING = 1;
    const STATUS_APPROVAL = 2;
    const STATUS_COMPLETED = 3;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getStatusText()
    {
        return [
            self::STATUS_CANCELED => __('Canceled'),
            self::STATUS_PENDING => __('Pending'),
            self::STATUS_APPROVAL => __('Approval'),
            self::STATUS_COMPLETED => __('Completed'),
        ][$this->status];
    }

    public function getStatusBootstrapClass()
    {
        return [
            self::STATUS_CANCELED => 'danger',
            self::STATUS_PENDING => 'secondary',
            self::STATUS_APPROVAL => 'warning',
            self::STATUS_COMPLETED => 'success',
        ][$this->status];
    }
}
