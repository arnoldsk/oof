<?php

namespace App;

class GameServerFtp
{
    protected $connection;

    protected function connect()
    {
        // Connect and login to FTP server
        $host = app()->config->get('ftp.host');
        $connection = ftp_connect($host);

        // Authenticate
        ftp_login(
            $connection,
            app()->config->get('ftp.username'),
            app()->config->get('ftp.password')
        );

        // Enable password usage
        ftp_pasv($connection, true);

        $this->connection = $connection;
    }

    protected function disconnect()
    {
        ftp_close($this->connection);
    }

    public function getDemoFiles()
    {
        $this->connect();
        $files = ftp_nlist($this->connection, '/csgo/*.dem');
        $this->disconnect();

        // Sort by name/date, descending
        arsort($files);

        return $files;
    }

    public function getDownloadedDemoFile($remoteFileBaseName)
    {
        // Create local demo public folder for downloads
        if (!is_dir(public_path('dem'))) {
            mkdir(public_path('dem'));
        }

        $demoFiles = $this->getDemoFiles();
        $remoteFilePath = null;

        foreach ($demoFiles as $demoFile) {
            if (mb_strpos($demoFile, $remoteFileBaseName) >= 0) {
                $remoteFilePath = $demoFile;
                break;
            }
        }

        if (!$remoteFilePath) {
            return false;
        }

        // Save the demo file
        $localFilePath = public_path('dem') . '/' . $remoteFileBaseName;

        if (!file_exists($localFilePath)) {
            $this->connect();
            ftp_get($this->connection, $localFilePath, $remoteFilePath, FTP_BINARY);
            $this->disconnect();
        }

        return $localFilePath;
    }
}
