<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';

    protected $fillable = [
        'user_id',
        'title',
        'content',
    ];

    protected $appends = [
        'titleSlug',
    ];

    public function getTitleSlugAttribute()
    {
        return slug($this->title);
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
