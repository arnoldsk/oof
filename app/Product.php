<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Payment;

class Product extends Model
{
    const MONTHLY_PREFIX_DATE_FORMAT = 'My';

    /**
     * Allows users to purchase products only once a month
     *
     * @var boolean
     */
    protected $useMonthlyPayments = true;

    /**
     * Allows adding discounts to prices based on authenticated user previous purchases
     *
     * @var boolean
     */
    protected $useTierProducts = true;

    protected $fillable = [
        'tier',
        'name',
        'description',
        'image',
        'price',
    ];

    protected $appends = [
        'discount',
        'canUserPurchase',
        'finalPrice'
    ];

    public function payments()
    {
        return $this->hasMany('App\Payment');
    }

    public function getCanUserPurchaseAttribute()
    {
        // Default is false so only logged in users can purchase
        $canPurchase = false;

        // If user hasn't purchased this yet
        if ($user = \Auth::user()) {
            $canPurchase = !$this->hasUserPurchased($user);
        }

        return $canPurchase;
    }

    public function getDiscountAttribute()
    {
        $rawPrice = $this->getOriginal('price');
        $discountAmount = 0;

        // Find all cheaper products that are purchased by the current logged in user and apply prices as a discount
        if ($this->useTierProducts && ($user = \Auth::user())) {
            $paymentsCollection = Payment::where('user_id', $user->id)
                ->where('amount', '<', $rawPrice)
                ->where('product_id', '!=', $this->id);

            // Apply monthly reset for discounts
            if ($this->useMonthlyPayments) {
                $paymentsCollection->whereMonth('created_at', \Carbon::now()->month);
            }

            $payments = $paymentsCollection->get();

            if (!empty($payments)) {
                foreach ($payments as $payment) {
                    if ($product = $payment->product) {
                        $discountAmount += $product->getOriginal('price');
                    }
                }
            }
        }

        return $discountAmount / 100;
    }

    public function getNameWithPrefix($contextModel = null)
    {
        $name = $this->getOriginal('name');
        $prefix = \Carbon::now()->format(self::MONTHLY_PREFIX_DATE_FORMAT);

        if ($contextModel && $contextModel->created_at) {
            $prefix = $contextModel->created_at->format(self::MONTHLY_PREFIX_DATE_FORMAT);
        }

        return sprintf('%s %s', $name, $prefix);
    }

    public function getPriceAttribute($price)
    {
        return $price / 100;
    }

    public function setPriceAttribute($price)
    {
        $this->attributes['price'] = $price * 100;
    }

    public function getFinalPriceAttribute($price)
    {
        return $this->price - $this->discount;
    }

    /**
     * Check if user has already bought the product at least once
     *
     * @param \App\User $user
     * @return boolean
     */
    public function hasUserPurchased(User $user)
    {
        // If user is not logged in we don't really know so allow
        if (!$user) {
            return false;
        }

        $paymentsCollection = Payment::where('product_id', $this->id)
            ->where('user_id', $user->id);

        // If monthly payments
        if ($this->useMonthlyPayments) {
            $paymentsCollection->whereMonth('created_at', \Carbon::now()->month);
        }

        return $paymentsCollection->count() > 0;
    }
}
