<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Product;

class UpdateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->integer('tier')->after('id');
        });

        $this->setProductTiers();
        $this->updateProductTierPrices();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

    /**
     * Set product tier value based on their price
     */
    protected function setProductTiers()
    {
        $products = Product::orderBy('price')->get();

        $tier = 1;
        foreach ($products as $product) {
            $product->update([
                'tier' => $tier,
            ]);
            $tier++;
        }
    }

    protected function updateProductTierPrices()
    {
        // We are still expecting there to be 4 tiers only so only 4 tier prices
        $prices = [
            1 => 1,
            2 => 2.5,
            3 => 5,
            4 => 10,
        ];

        $products = Product::orderBy('price')->get();

        $tier = 1;
        foreach ($products as $product) {
            $product->update([
                'price' => $prices[$tier],
            ]);
            $tier++;
        }
    }
}
