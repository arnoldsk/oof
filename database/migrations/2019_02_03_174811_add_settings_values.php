<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Setting;
use App\Product;

class AddSettingsValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tiers = Product::groupBy('tier')->pluck('tier');

        foreach ($tiers as $tier) {
            $path = sprintf('product_tier_%s_perks', $tier);

            if (!Setting::where(['path' => $path])->count()) {
                Setting::create([
                    'path' => $path,
                    'value' => '',
                ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
