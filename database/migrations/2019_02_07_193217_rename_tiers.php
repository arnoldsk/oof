<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Product;
use App\Setting;

class RenameTiers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->renameProducts();
        $this->renameSettings();
    }

    protected function renameProducts()
    {
        $products = Product::orderBy('price')->get();
        $descriptions = [
            1 => 'Tier 1 option, can be upgraded from any previous tier.',
            2 => 'Tier 2 option, can be upgraded from Bronze and Silver.',
            3 => 'Tier 3 option, can be upgraded from Bronze.',
            4 => 'Tier 4 option, can be upgraded to higher tiers.',
        ];

        $tier = $products->count();
        foreach ($products as $product) {
            $product->update([
                'description' => $descriptions[$tier],
                'tier' => $tier,
            ]);
            $tier--;
        }
    }

    protected function renameSettings()
    {
        $settings = Setting::where('path', 'like', 'product_tier_%')->orderBy('path')->get();

        $tier = $settings->count();
        foreach ($settings as $setting) {
            $setting->update([
                'path' => sprintf('product_tier_%s_perks', $tier),
            ]);
            $tier--;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
