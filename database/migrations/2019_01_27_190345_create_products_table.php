<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->string('image')->nullable();
            $table->integer('price');
            $table->timestamps();
        });

        $this->populateTierProducts();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }

    /**
     * Create initial products
     * 
     * @return void
     */
    protected function populateTierProducts()
    {
        $items = [[
            'name' => 'Bronze Supporter',
            'description' => 'Tier 1 option, can be upgraded to higher tiers.',
            'image' => null,
            'price' => 100,
        ], [
            'name' => 'Silver Supporter',
            'description' => 'Tier 2 option, can be upgraded from Bronze.',
            'image' => null,
            'price' => 500,
        ], [
            'name' => 'Gold Supporter',
            'description' => 'Tier 3 option, can be upgraded from Bronze and Silver.',
            'image' => null,
            'price' => 1500,
        ], [
            'name' => 'Diamond Supporter',
            'description' => 'Tier 4 option, can be upgraded from any previous tier.',
            'image' => null,
            'price' => 3000,
        ]];

        foreach ($items as $item) {
            DB::table('products')->insert($item);
        }
    }
}
