<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Home routes
 */
Route::get('/', 'HomeController@index')->name('home');
Route::get('/news/{id}/{titleSlug}', 'HomeController@news')->name('home.news')->where(['id' => '[0-9]+']);
Route::get('/demos/{filename?}', 'HomeController@demos')->name('home.demos');
Route::get('/discord', 'HomeController@discord')->name('discord');
Route::get('/steamgroup', 'HomeController@steamgroup')->name('steamgroup');
Route::get('/users', 'HomeController@users')->name('users');

/**
 * Profile
 */
Route::get('/profile/{userId?}', 'ProfileController@index')->name('profile')->where(['userId' => '[0-9]+']);
Route::post('/profile/discord/update', 'ProfileController@discordUpdate')->name('profile.discord.update');
Route::get('/profile/discord/return', 'ProfileController@discordReturn')->name('profile.discord.return');

/**
 * Admin routes
 */
Route::get('/admin/users', 'AdminController@users')->name('admin.users');
Route::get('/admin/payments/{userId?}', 'AdminController@payments')->name('admin.payments')->where(['userId' => '[0-9]+']);
Route::get('/admin/donations/{userId?}', 'AdminController@donations')->name('admin.donations')->where(['userId' => '[0-9]+']);
Route::get('/admin/news/{id?}', 'AdminController@news')->name('admin.news')->where(['id' => '[0-9]+']);
Route::post('/admin/news/save', 'AdminController@newsSave')->name('admin.news.save');
Route::post('/admin/news/delete', 'AdminController@newsDelete')->name('admin.news.delete');
Route::get('/admin/settings', 'AdminController@settings')->name('admin.settings');
Route::post('/admin/settings/save', 'AdminController@settingsSave')->name('admin.settings.save');

/**
 * Authentication
 */
Route::get('/auth', 'AuthController@index')->name('auth');
Route::get('/auth/steam', 'AuthController@redirectToSteam')->name('auth.steam');
Route::get('/auth/steam/handle', 'AuthController@handle')->name('auth.steam.handle');
Route::post('/logout', 'AuthController@logout')->name('auth.logout');

/**
 * Donations
 */
Route::get('/donate', 'DonateController@index')->name('donate');
Route::post('/donate/start', 'DonateController@start')->name('donate.start');
Route::get('/donate/paypal/completed', 'DonateController@paypalCompleted')->name('donate.paypal.completed');
Route::get('/donate/paypal/cancel', 'DonateController@paypalCancel')->name('donate.paypal.cancel');
Route::post('/donate/paypal/notify', 'DonateController@paypalNotify')->name('donate.paypal.notify');

/**
 * Payments
 */
Route::get('/shop', 'ShopController@index')->name('shop');
Route::get('/shop/supporters', 'ShopController@supporters')->name('shop.supporters');
Route::post('/shop/payment/create', 'ShopController@paymentCreate')->name('shop.payment.create');
Route::post('/shop/payment/execute', 'ShopController@paymentExecute')->name('shop.payment.execute');
